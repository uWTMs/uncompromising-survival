ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE

--	[ 		Wickerbottom Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "We'll need the body for a heart to be of much use."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Winona dear, is there documentation for this model?"
    ANNOUNCE.ANNOUNCE_RATRAID = "Ah, new subjects of study!"
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Well well, aren't you well grown!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Oh! An excellent chance for a lesson in tracking!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "The rain is polluted with an acidic substance!",
        "Shelter! I must find shelter from this corosive weather!",
        "The rain must be heavily polluted!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "My, what an invasive species!"

    DESCRIBE.UNCOMPROMISING_RAT = "Rodentia kleptomanis, and quite the large specimen!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "One entrance to a no doubt vast tunnel system!"
    DESCRIBE.RATPOISONBOTTLE = "The toxic elemnts of the fungus have been fully activated."
    DESCRIBE.RATPOISON = "A minor chemical odor emanating from the mixture attracts rodents."

    DESCRIBE.MONSTERSMALLMEAT = "A small quantity of dubiously sourced meat."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Somewhat prepared, but still not entirely safe for consumption."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Salty and strange."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "Might that strange vegitation be the cause of the polluted rain?"
    DESCRIBE.TOADLING = "They appear subservient to the nearby fungi."
	
    DESCRIBE.GASMASK = "For investigating dangerous areas."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "A Royal Arachnic, posthumous."
	ANNOUNCE.ANNOUNCE_SNEEZE = "Ahh... CHOOOOOOOOO!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Sniff sniff. Must be hayfever season."
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Ahh... Finally I can breathe."
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"It's never quiet around this place!",
		"This heat is getting MORE unusual.",
		"Watch your heads!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "I should chop myself free!"
	DESCRIBE.SAND = "Sand always seems to find its way into my books."
	DESCRIBE.SANDHILL = "The dune's iconic shape is formed by the winds."
	DESCRIBE.SNOWPILE = "Makes one think of stories by the fire during a stormy night."
	DESCRIBE.SNOWGOGGLES = "For winter reading? ...Perhaps not."
	
	DESCRIBE.SNOWMONG = "I have no idea."
	DESCRIBE.SHOCKWORM = "A highly conductive Annelida!"
	DESCRIBE.ZASPBERRY = "Nonsense, there's no such thing as a blue Rubus."
	DESCRIBE.ICEBOOMERANG = "A severed appendage that dramatically lowers body temperature on contact."
	DESCRIBE.SNOWBALL_THROWABLE = "A handful of snow, mashed together so it can be thrown."
	DESCRIBE.VETERANSHRINE = "That's certainly not the best idea."

	DESCRIBE.BUSHCRAB = "Ardisia brachyura!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "I shall put its mutative properties to good use."