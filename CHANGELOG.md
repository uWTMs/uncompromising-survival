![Uncompromising Logo](images/logo.png)

# Currently Confirmed Change-list:

* [x] *(checked means tested, working and delivered)*

* [ ] *(unchecked means planned, added to roadmap and dev assigned to work on it)*

### DST Uncompromising Mod Collab - Alpha version 1.0.0 - Uncompromising Start

* [x]  Knockback mechanic on some bosses

* [ ]  Hardcore mode - disable rollback

* [ ]  Hardcore mode - disable movement on death

* [ ]  Hardcore mode - disable ghost on death, body lays on ground and accepts revives (similar to the forge)

* [ ]  Hardcore mode - Auto amulet revive (like in normal Don't Starve)

* [x]  Applied horticulture recipe nerf (uses 1 leafy meat + 1 bucket o poop instead of 1 seeds + 1 poop)

* [x]  [Settings menu](https://i.imgur.com/N3rLMpH.png)

* [x]  [Totally Normal Livingtree root attack](https://imgur.com/8gC0cdv.gif)

* [x]  [Custom art for livingtree by @ Advent](https://cdn.discordapp.com/attachments/626788873942335517/628233540169105408/up.gif)

* [x]  Bishop flees from attacks after shooting

* [x]  New shadow creatures (Dread Eye & Creeping Fear)

* [x]  Tweaked sanity ranges for shadow creature appearance

* [x]  Statue walls, signs, lava ponds, bone fragments hitbox pathing is ignored (now they have no collision)

* [x]  Butterfly wing nerf - Health restored to 5 (down from 8)

* [x]  Meatballs nerf - Hunger restored to 50 (down from 62.5)

* [x]  Stone fruit growth nerf - Stone fruit bush base growth duration tripled
 
* [x]  Ice and twigs cannot be more than 2 inside crockpot recipes
 
* [x]  [Rat waves](https://cdn.discordapp.com/attachments/623666104937480192/629152866426290197/damnrat.gif), that will periodically come after day 50, they mess around your items, eat your food, and bite you for max health penalties 
 
* [x]  Carrots are rarer in world generation

* [x]  Berry bushes are rarer in world generation
 
* [ ]  During Worldgen, carrots can sometimes be replaced with different vegetables. (Asparagus, Potato, Toma Root, etc etc) 

* [x]  Mod settings for disabling/enabling features

* [x]  Pigs and bunnies defend their houses when hammered

* [x]  Pierogi spoils in 10 days instead of 20

* [x]  Cooked Bird egg loop removed

* [x]  Bird's stomach can only withstand 4 monster meat before dying out (vary her diet with normal meat to cure it)

* [x]  WX will take more damage from rain (3x)

* [x]  Moonrock idol requires 5 moonstone

* [x]  Cave entrances and batcaves spawn 3x the bats
 
* [x]  Woodie were-Goose gets wet when running on water

* [x]  [Project logo](images/logo.png) added, by @ Ismael Daniel

* [x]  Monster meat must be dilluted with normal meat in crocpot to not make lasagna, with the exception of monster jerky

* [x]  Monster meat has a default monster value of 2.5 now. Cooked is 2.0 monster value. Monster Jerky is 1.0 monster value. This means Monster jerky can still make meatballs.

* [x]  Wolfgang sanity drain to 1.5x (prev was 1.1x)

* [x]  Catcoon have increased health

* [x]  Cactus, stone fruits and spiky bushs don't grow in winter

* [x]  Pig guards now break free by attacking walls that entrap them

* [x]  Bee box honey yield is 1,2,4 instead of 1,3,6

* [x]  Moosegoose's nest has had its carrots eaten, and some berry bushes disappeared too. Moosling's might've been hungry.

* [x]  Bacon and eggs has a lower cooking priority than monster lasagna

* [x]  Fire and ice hounds have fire and ice attacks on their bite

* [x]  Walrus has more health and ice doggos

* [x]  Haunting pig torches has a low chance of spawning a pig guard now

* [x]  Bee Queen now has an AOE that ignores hives and bees

* [x]  Werepigs prefer attacking over eating

* [x]  Koelefants drop 4 meat now, instead of 8

* [x]  Marble armor ignores our knockback mechanic

* [x]  Bees don't drop honey, only stingers

* [x]  Bacon and eggs spoil time is halved, to 10 days

* [ ]  Added setting to increase nr of hounds per wave (default: 1.5x)

* [x]  Food appearance rates reduced significantly (berries, carrots, mushrooms)

* [x]  Bunnymen no longer drop carrots

* [x]  Only winona can use her generators

* [x]  Pengulls are territorial and attack now if you get too close

* [x]  Wormwood takes more fire damage, for more time

* [x]  Add Treeguard AOE

* [x]  Weather protective clothing will become less effective at lower percents (75%-50%-25%)

* [x]  Sewing Kits repair 50% less, but have double the uses. (Makes it easier to keep clothing in acceptable durability levels)

* [x] Hayfever added to spring

* [x] Added Gasmask, Antihistamine foods, and Mini Fan changes to combat hayfever [Wormwood is immune to Hayfever!]

* [x] Increased Mini Fan cost to compensate for new function

* [x] Fire Meteor's will rain from the sun after the Antlion summons sinkholes. She now has the power of the sun. Have fun.

* [x] Dragonfly and Gmoose seasonal spawning

* [x] Treeguard Root Attack

* [x] Hounds, Spiders, and Pengulls will mutate on full moons

* [x] Spiderqueen leaves a corpse on death, on a full moon it will turn into a shattered spider nest

* [x] Snowstorms, snowstorm overlay, snowpiles

* [x] Snowpiles form around players during snowstorms, can be destroyed by hand or with a shovel

* [x] Fashion and Desert Goggles are now a UNIVERSAL CRAFTING RECIPE! No more rng oasis fishing.

* [x] New Catcoon Cap Goggles hat, same effect as Desert Goggles, increased insulation over the Cat Cap

* [x] Standing near a fire, walls, many trees, using a torch, or either variation of goggles will protect against snowstorms

* [x] Deerclops "enrages" at half health

* [x] New mob: Abominamole, spawns out of snow piles

* [x] New mob: Depths Eel, added to Depth Worm attacks.

* [x] Zaspberries: berry drops from depth eels that give the player the chaud-froid effect for less time and make the player glow

* [x] Mini-fan cost increased, now protects against hayfever

* [x] Honey foods, most soups, and onions delay hayfever

* [x] Moose range increased, now does aoe damage to non-mossling units

* [x] Mutated Hounds attack faster and move faster

* [x] Increased Catcoon Tail drop rate

* [x] Ruins Pillars can now be destroyed by the Ancient Guardian

* [x] The Ancient Guardian fight has been improved, now has a stomp attack that shakes stone slabs from the ceiling, ramming into large ones will stun him

* [x] Spiders now have a leap attack and drop monster morsels

* [x] Added Bush Crabs

* [x] More shadows spawns on insane players

* [x] Killing shadows regains 10 sanity instead of 15

* [x] Earthquake debris during summer has a chance to split the earth, creating a flame geyser. Lavae will spawn from the geysers

* [x] Killing the Bee Queen will stop hayfever until the Queen Hive respawns

* [x] Telltale Hearts now cost human meat, which is found inside skeletons

* [x] Wendy's healing tonic recipe changed to use 50 health instead of a telltale heart

* [x] Small chance to spawn mosquito's when picking juicy berry bushes

* [x] The Toadstool will now occasionally spawn mushroom sprouts on the overworld, these sprouts will cause constant, acid rain, which will burn the player and turn plants diseased

* [x] NEW MOB: Toadlings, large Toad creatures who will spawn with and defend the mushroom sprouts from being chopped down

* [x] Sickly Toads will fall from the sky if the sprouts are left alone for long enough

* [x] Cactus Flesh, Cooked Cactus Flesh, and Cooked Green Caps now deal slightly more damage when eaten

* [x] Decreased time between disease chance checks

* [x] Added Moon Oil, a cure for diseased plants

* [x] Added Frozen Pincer an ice boomerang that rarely drops from abominamoles (stats pending)

* [x] Snowballs added: makes targets cold when thrown, can be used to freeze targets

### To check current things worked on: Visit [#mod_roadmap](https://discordapp.com/channels/623649948130344960/624590758959382539) on Discord

### To vote on features: Visit [#mod_polls](https://discordapp.com/channels/623649948130344960/623661746216763393) on Discord

### To submit your own features: Visit [#mod_submissions](https://discordapp.com/channels/623649948130344960/623661395539656704) on Discord

### Our internal spreadsheet with development and testing status can be found [here](https://docs.google.com/spreadsheets/d/1UEPA2O0gz0agyrjvCe9k1K-_gipPyTz3B2mHsEYCUhM/edit?usp=sharing)