ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WOODIE
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WOODIE.DESCRIBE

--	[ 			Woodie Descriptions			]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Wouldn't do much good without a body, eh?"
    ANNOUNCE.ANNOUNCE_WINONAGEN = "I'm not too keen on maintaining an electrical engine."
    ANNOUNCE.ANNOUNCE_RATRAID = "That's the sound of somethin' devious."
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Quick, grab the logs!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Luce'! Hang on tight!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "We gotta get outta this confounded rain!",
        "L-Lucy?! You got a drop on you!",
        "Don't worry Luce', I got you.",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Wha-- Oh! Look Luce', something new to chop!"

    DESCRIBE.UNCOMPROMISING_RAT = "More vermin come to steal my kit!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "That's where the varments are shackin' up!"
    DESCRIBE.RATPOISONBOTTLE = "Feast your eyes on the deep woods gut bomb!"
    DESCRIBE.RATPOISON = "A little somethin' for the moochers' next visit..."

    DESCRIBE.MONSTERSMALLMEAT = "Might be useful if it was bigger..."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "That's no Canadian Kosher."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "That's no Canadian Kosher."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "That cap is killing my trees!"
    DESCRIBE.TOADLING = "Hop off, ya hoser."
	
    DESCRIBE.GASMASK = "Keeps my lungs clear."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "That's a sight for sore eyes, eh?"
	ANNOUNCE.ANNOUNCE_SNEEZE = "Achoo! Excuse me. Sorry."
	ANNOUNCE.ANNOUNCE_HAYFEVER = "You have allergies like me, Lucy?"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Ah! Clean air again."
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Cool it, hozer!",
		"It's raining fire! Save the trees!",
		"This is nothing like Canadian weather!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "You're up, Lucy!"
	DESCRIBE.SAND = "Sandy."
	DESCRIBE.SANDHILL = "It's a big pile of sand."
	DESCRIBE.SNOWPILE = "You call that being snowed in?"
	DESCRIBE.SNOWGOGGLES = "Now I feel at home."
	
	DESCRIBE.SNOWMONG = "The bumble sinks!"
	DESCRIBE.SHOCKWORM = "That worm's lit up like the Northern lights!"
	DESCRIBE.ZASPBERRY = "Blue raspberry is not a real berry."
	DESCRIBE.ICEBOOMERANG = "This is just Canada on a stick, eh?"
	DESCRIBE.SNOWBALL_THROWABLE = "I'm an expert on snowball fights!"
	DESCRIBE.VETERANSHRINE = "It makes my skin crawl. eh?"

	DESCRIBE.BUSHCRAB = "Farm from a freshwater."
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "Aboot time the moon started doing me some favors."