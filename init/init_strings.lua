STRINGS = GLOBAL.STRINGS

-- [              DSTU Related Overrides                  ]

STRINGS.DSTU = {
	ACID_PREFIX =
    {
        NONE = "",
        GENERIC = "Corroding",
        RABBITHOLE = "",
        CLOTHING = "Eroding",
        FUEL = "Caustic",
        TOOL = "Rusting",
        FOOD = "Sour",
        POUCH = "Deteriorating",
        WETGOOP = "Toxic",
    },
}
