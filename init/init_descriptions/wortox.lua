ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WORTOX
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WORTOX.DESCRIBE

--	[ 		Wortox Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Just kidding! Hyuyu."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Let the mortal have her fun. I've elsewhere to run!"
    ANNOUNCE.ANNOUNCE_RATRAID = "The piper calls! Best hide it all!"
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Hyuyu, what a jest! Come at me, pests!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Easy, too easy! Your pranks are cheesy!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "Aerial attack! I'm in need of a shack!",
        "A sour turn, the water burns!",
        "Ouch! Hyuyu!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Surprise, surprise! What a thing to rise!"

    DESCRIBE.UNCOMPROMISING_RAT = "You won't run off with my flute again, hm?"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "A portal to the little pranksters' den."
    DESCRIBE.RATPOISONBOTTLE = "My oh my, what a way to die!"
    DESCRIBE.RATPOISON = "A deadly slick! That should do the trick!"

    DESCRIBE.MONSTERSMALLMEAT = "What a monstrous little fiend you were!"
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "No matter how prepared, it's just not my fare."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Ho, most grim."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "It seems the source of that malicious mycelium!"
    DESCRIBE.TOADLING = "Come to hop away...or perhaps to play?"
	
    DESCRIBE.GASMASK = "Freezes sneezes!"
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "There is no bliss for this carcass!"
	ANNOUNCE.ANNOUNCE_SNEEZE = "Achooo! Hyuyu!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Dust itself is pranking my snout!"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "It is relieving, for there is no more sneezing!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Frightening!",
		"Fiery!",
		"Fury!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "You can't hold me down!"
	DESCRIBE.SAND = "Itchy and scratchy!"
	DESCRIBE.SANDHILL = "Great grains of gravel!"
	DESCRIBE.SNOWPILE = "Ooh what mischief we could cause with this! Hyuyu!"
	DESCRIBE.SNOWGOGGLES = "Cats Eyes!"
	
	DESCRIBE.SNOWMONG = "What large teeth you have!"
	DESCRIBE.SHOCKWORM = "This one is sparking!"
	DESCRIBE.ZASPBERRY = "This will give someone quite a shock!"
	DESCRIBE.ICEBOOMERANG = "They cease their attacks, by being stopped in their tracks!"
	DESCRIBE.VETERANSHRINE = "It's not a prank if it's on yourself!"
	DESCRIBE.SNOWBALL_THROWABLE = "Hyuyu! I like this game!"

	DESCRIBE.BUSHCRAB = "Ohoho, what a trickster!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "Stink-b-gone!"