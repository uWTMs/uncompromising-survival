ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WAXWELL
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WAXWELL.DESCRIBE

--	[ 		Maxwell Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "I'll have to impart this heart to their rotting corpse."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "I'm more of a magic and dark arts kind of guy."
    ANNOUNCE.ANNOUNCE_RATRAID = "Oh heavens no."
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "...come now Charlie, is this really nessecary?"
    -- Yes. Enjoy.
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "How dare you? Get back here!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "Ow! The rain is burning me!",
        "My suit! It's ruined!",
        "I'm melting! MELTING!!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Vile spore-spawn!"

    DESCRIBE.UNCOMPROMISING_RAT = "Filthy vermin!"
    DESCRIBE.UNCOMPROMISING_RATHOLD = "I don't recall granting you vermin permission to settle in my world."
    DESCRIBE.RATPOISONBOTTLE = "Hmhm...I remember what the red ones were for now."
    DESCRIBE.RATPOISON = "A little something to keep the vermin at bay."

    DESCRIBE.MONSTERSMALLMEAT = "Fare from the lowest of vermin."
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "What an unpleasant prospect..."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "My teeth are getting too old for this."

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "It seems to be polluting the air with spores."
    DESCRIBE.TOADLING = "They've been overtaken by some sort of fungal hive-mind."
	
	DESCRIBE.GASMASK = "This clears the vapors."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Is it waiting for something?"
	ANNOUNCE.ANNOUNCE_SNEEZE = "ah...CHOOOO!"
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Ugh. Allergies."
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Ah... I can breathe again."
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Never a moment of peace.",
		"That mangy thing is getting worked up.",
		"That's probably not good.",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "It's roots have taken hold!"
	DESCRIBE.SAND = "Rocks so thoroughly beaten down they are no longer recognizable."
	DESCRIBE.SANDHILL = "A little mound of yellow sand."
	DESCRIBE.SNOWPILE = "Remove it, before it gets in my boots."
	DESCRIBE.SNOWGOGGLES = "..."
	
	DESCRIBE.SNOWMONG = "It's about as smart as it looks."
	DESCRIBE.SHOCKWORM = "I definitely did not make that one!"
	 -- ...
	DESCRIBE.ZASPBERRY = "I'm not one for wild berries, magical or not."
	DESCRIBE.ICEBOOMERANG = "Magic is great, isn't it?"
	DESCRIBE.SNOWBALL_THROWABLE = "Not. The. Suit. What do you not get about this?"
	DESCRIBE.VETERANSHRINE = "I already have fuel-related problems, what's another?"

	DESCRIBE.BUSHCRAB = "I guess they've finally hatched."
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "I squeezed every last drop of oil from that rock."