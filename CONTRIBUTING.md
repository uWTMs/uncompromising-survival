# Uncompromising Team

## Head Developers & Founders

- @ ADM

- @ Canis

- @ Sho / Scrimbles

## Code contributors

- @ ADM

- @ Hornet

- @ Norfeder

- @ loopuleasa


- @ Sho / Scrimbles

## Art contributors

- @ Advent - Living tree root attack custom animation

- @-Variant- - Rat artwork

- @ Ismael Daniel - Uncompromising logo

- @ Maradyne - Logo animation, Rat Den art

## Quote contributors

- @ Maradyne - Quote quality assurance

- @ Canis

&nbsp;

Many thanks to the people on discord with the design, votes and discussions on what should make it in the mod collab.