ANNOUNCE = GLOBAL.STRINGS.CHARACTERS.WURT
DESCRIBE = GLOBAL.STRINGS.CHARACTERS.WURT.DESCRIBE

--	[ 		Wurt Descriptions		]	--

    ANNOUNCE.ANNOUNCE_HARDCORE_RES = "Not go in there, florp."
    ANNOUNCE.ANNOUNCE_WINONAGEN = "Weeno-lady need to do it!"
    ANNOUNCE.ANNOUNCE_RATRAID = "What that squeak?"
    ANNOUNCE.ANNOUNCE_RATRAID_SPAWN = "Sneaky things!"
    ANNOUNCE.ANNOUNCE_RATRAID_OVER = "Little meanies take stuff! Follow! Get!"
    ANNOUNCE.ANNOUNCE_ACIDRAIN = {
        "Ow! Burny rain!",
        "Hurting! Rain hurting!",
        "Ow! This rain bad!",
    }
    ANNOUNCE.ANNOUNCE_TOADSTOOLED = "Aah! Mushies! Big mushies!"

    DESCRIBE.UNCOMPROMISING_RAT = "Spiderfolk family? Have hair."
    DESCRIBE.UNCOMPROMISING_RATHOLD = "Little eyes in hole! Hello!"
    DESCRIBE.RATPOISONBOTTLE = "Veggies but taste really bad."
    DESCRIBE.RATPOISON = "Spill! Wasn't good anyway."

    DESCRIBE.MONSTERSMALLMEAT = "No, not good. Leave for Spiderfolk?"
    DESCRIBE.COOKEDMONSTERSMALLMEAT = "Glurgh! Smell so bad."
    DESCRIBE.MONSTERSMALLMEAT_DRIED = "Even bader now, florpt!"

    DESCRIBE.MUSHROOMSPROUT_OVERWORLD = "Mushroom? Mad mushroom!"
    DESCRIBE.TOADLING = "Veggie...froggie? Not sure if ok to eat."
	
    DESCRIBE.GASMASK = "Smell bad, smells bad."
	DESCRIBE.MOCK_DRAGONFLY = DESCRIBE.DRAGONFLY
	DESCRIBE.SPIDERQUEENCORPSE = "Like the smell!"
	ANNOUNCE.ANNOUNCE_SNEEZE = "Choo! Egh."
	ANNOUNCE.ANNOUNCE_HAYFEVER = "Nose feel funny!"
	ANNOUNCE.ANNOUNCE_HAYFEVER_OFF = "Nose feels good again!"
	ANNOUNCE.ANNOUNCE_FIREFALL = {
		"Sky is scary!",
		"Anty angry!",
		"Fire! Florp!",
	}
	ANNOUNCE.ANNOUNCE_ROOTING = "Stuck!"
	DESCRIBE.SAND = "Grainy stuffs."
	DESCRIBE.SANDHILL = "Itch on scales..."
	DESCRIBE.SNOWPILE = "Fun! Brr..."
	DESCRIBE.SNOWGOGGLES = "Silly eyes on silly hat!"
	
	DESCRIBE.SNOWMONG = "Not fun!!!"
	DESCRIBE.SHOCKWORM = "Sparky Worm!!"
	DESCRIBE.ZASPBERRY = "Raps-perry?"
	DESCRIBE.ICEBOOMERANG = "This is fun, florp!"
	SNOWBALL_THROWABLE = "Why you throw at us!?"
	DESCRIBE.VETERANSHRINE = "I hate it, florp!"

	DESCRIBE.BUSHCRAB = "Crabby!"
	DESCRIBE.LAVAE2 = DESCRIBE.LAVAE
	DESCRIBE.DISEASECUREBOMB = "Plant make snacks in no time!"